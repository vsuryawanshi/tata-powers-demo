import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class WatermarkComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">Watermark Detection</div>
                {
                    this.props.data.results ?
                    <div>
                        <div className="res-item half">
                            <div className="res-label">{this.props.data.results[0][0]}</div>
                            <div className="res-text">
                                {
                                    " (" + (this.props.data.results[0][1]).toFixed(1) + "%)"
                                }
                            </div>
                            <div className="res-val">
                                <LinearProgress value={this.props.data.results[0][1]/100}/>
                            </div>
                        </div>
                    </div> 
                    :
                    null       
                }
                
            </div>
        )
    }
}