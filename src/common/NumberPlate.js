import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class NumberPlateComp extends Component{
    constructor(props){
        super(props);

        this.state = {
            recs:[]
        }

        this.imageContainer = null
    }

    componentDidMount(){
        setTimeout(()=>{
            this.drawRectangles();
        },100);
    }

    drawRectangles(){
        if(this.props.data.length > 0) {
            var contWidth = this.imageContainer.clientWidth;
            var contHeight = this.imageContainer.clientHeight;
            var originalWidth = this.props.parentCoordinates.w;
            var originalHeight = this.props.parentCoordinates.h;
            
            var recWidth = this.props.data[2][2] - this.props.data[2][0];
            var recHeight = this.props.data[2][3] - this.props.data[2][1];

            var scaleX = contWidth/originalWidth;
            var scaleY = contHeight/originalHeight;

            var _x = scaleX * this.props.data[2][0];
            var _y = scaleY * this.props.data[2][1];

            var newRecWidth = scaleX * recWidth;
            var newRecHeight = scaleY * recHeight;

            var tempObj = {
                top:_y,
                left:_x,
                width:newRecWidth,
                height:newRecHeight
            }
            var temp = [];
            temp.push(tempObj);
            this.setState({
                recs:temp
            });
        }
    }

    componentDidUpdate(prevProps){
        setTimeout(()=>{
            this.drawRectangles();
        },100);
    }

    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">Number Plate Detection</div>
                {
                    this.props.data.length > 0 ?
                        <div className="res-item half">
                            <div className="res-label">{"License Plate"}</div>
                            <div className="res-text">
                                {this.props.data[0]}
                                <span>
                                    {
                                        " (" + (this.props.data[1]).toFixed(1) + "%)"
                                    }
                                </span>
                            </div>
                            <div className="res-val">
                                <LinearProgress value={this.props.data[1]/100}/>
                            </div>
                        </div>
                    :
                    null       
                }
                <br/>
                <br/>
                <div className="img-contee" ref={node => this.imageContainer = node}>
                    <img className="imgww" src={this.props.img}/>
                    <div className="mask">
                        {
                            this.state.recs.map((rec,i)=>{
                                return(
                                    <div className="rec" key={i} style={{top:rec.top,left:rec.left,width:rec.width,height:rec.height}}/>
                                )
                            })
                        }
                    </div>
                </div>
                
            </div>
        )
    }
}