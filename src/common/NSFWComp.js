import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class NSFWComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">NSFW Detection</div>
                <div className="res-item ">
                    <div className="res-label">{"Rating"}</div>
                    <div className="res-text">
                        {this.props.data.rating}
                    </div>
                </div>
                {
                    this.props.data.general ?
                    <div>
                        {
                            this.props.data.general.map((sc,idx)=>{
                                return(
                                    <div className="res-item half" key={idx}>
                                        <div className="res-label">{sc[0]}</div>
                                        <div className="res-text">
                                            {
                                                " (" + (sc[1] * 100).toFixed(1) + "%)"
                                            }
                                        </div>
                                        <div className="res-val">
                                            <LinearProgress value={sc[1]}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div> 
                    :
                    null       
                }
                
            </div>
        )
    }
}