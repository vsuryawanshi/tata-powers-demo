import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

const ALLOWED = ['body','orientation','color','make','year','model'];
export default class CarModelComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">Car Model Detection</div>
                {
                    this.props.data ?
                    <div>
                        {
                            Object.keys(this.props.data).map((okey,i)=>{
                                if(ALLOWED.indexOf(okey) !== -1){
                                    let currentObj = this.props.data[okey];
                                    return(
                                        <div className="res-item half" key={i}>
                                            <div className="res-label">{okey}</div>
                                            <div className="res-text">
                                                {currentObj[0]}
                                                <span>
                                                    {
                                                        " (" + (currentObj[1]).toFixed(1) + "%)"
                                                    }
                                                </span>
                                            </div>
                                            <div className="res-val">
                                                <LinearProgress value={currentObj[1]/100}/>
                                            </div>
                                        </div>
                                    )
                                    
                                }
                            })
                        }
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}