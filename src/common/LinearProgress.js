import React, { Component } from 'react';

export default class LinearProgress extends Component {
    render() {
        let percentValue = (this.props.value >= 1 ? 100 : this.props.value*100);
        let color = "#fff";
        if(percentValue < 40){
            color = "#ea8181";
        } else if(percentValue >= 40 && percentValue < 75){
            color = "#f5f029";
        } else {
            color = "#2ab32a";
        }
        return (
            <div className="linear-progress-wrap">
                <div className="total"/>
                <div className="prog" style={{"width":percentValue+"%","backgroundColor":color}}/>
            </div>
        );
    }
}