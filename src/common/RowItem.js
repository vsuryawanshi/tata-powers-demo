import React from "react";
import LinearProgress from "./LinearProgress";

export default class RowItem extends React.Component{
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.drawImages();
    }

    componentDidUpdate(newProps){
        var ctx = this.canvas.getContext("2d");
        ctx.clearRect(0,0,this.wrap.clientWidth, this.wrap.clientHeight);
        this.drawImages();
        return true;
    }

    drawImages(){
        this.canvas.width = this.wrap.clientWidth;
        this.canvas.height = this.wrap.clientHeight;

        var targetWidth = Math.abs(this.props.data[3] - this.props.data[2]);
        var targetHeight = Math.abs(this.props.data[5] - this.props.data[4]);

        var imageAspectRatio = targetWidth/targetHeight;
        var canvasAspectRatio = this.canvas.width/this.canvas.height;
        var renderableHeight, renderableWidth;


        if(imageAspectRatio < canvasAspectRatio){
            renderableHeight = this.canvas.height;
		    renderableWidth = targetWidth * (renderableHeight / targetHeight);
        } else if (imageAspectRatio > canvasAspectRatio){
            renderableWidth = this.canvas.width
		    renderableHeight = targetHeight * (renderableWidth / targetWidth);
        } else {
            renderableHeight = this.canvas.height;
		    renderableWidth = this.canvas.width;
        }
        
        const context = this.canvas.getContext("2d");
        context.drawImage(this.props.imageRef, this.props.data[2], this.props.data[4], targetWidth, targetHeight,0, 0, renderableWidth,renderableHeight);
    }

    render(){
        let percent = parseFloat(this.props.data[1]);
        return(
            <div className="row-cont">
                <div className="top">
                    <div className="row-lbl">{this.props.data[0]}</div>   
                    <div className="pval">(Accuracy : {percent + "%"})</div>
                </div>
                <div className="row-prg">
                    <LinearProgress value={percent/100} />
                </div>
                <div className="canvas-wrap" ref={node => this.wrap = node}>
                    <canvas ref={node => this.canvas = node}/>
                </div>
            </div>
        )
    }
}