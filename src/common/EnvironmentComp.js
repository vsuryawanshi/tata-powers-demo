import React, {Component} from "react";

export default class EnvironmentComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="res-item">
                    <div className="res-label">Type of Environment</div>
                    <div className="res-val"><div className="rtag">{this.props.data.typeOfEnvironment}</div></div>
                </div>
            </div>
        )
    }
}