import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class ImageMatchComp extends Component{
    constructor(props){
        super(props);

        this.state = {
            selectedItem:-1
        }
    }


    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">Image Matching</div>
                {
                    this.props.data.length > 0 ?
                    <div>
                        {
                            this.props.data.map((sc,idx)=>{
                                return(
                                    <div className="res-item half bw" key={idx}>
                                        <div className="res-label" onClick={(e)=>{
                                        if(this.state.selectedItem == -1 || this.state.selectedItem !== idx){
                                            this.setState({
                                                selectedItem:idx
                                            })
                                        } else {
                                            this.setState({
                                                selectedItem:-1
                                            })
                                        }
                                    }}>{sc[0].substring(0,50)}</div>
                                        <div className="res-text">
                                            {
                                                " (" + (sc[1]).toFixed(1) + "%)"
                                            }
                                        </div>
                                        <div className="res-val">
                                            <LinearProgress value={sc[1]/100}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    :
                    <div style={{marginTop:40,textAlign:"center"}}>No Matching images</div>
                }

                {
                    this.state.selectedItem !== -1 ?
                    <div className="img-contee">
                        <img className="imgww" src={this.props.data[this.state.selectedItem][0]}/>
                    </div>
                    :
                    null
                }
                
            </div>
        )
    }
}