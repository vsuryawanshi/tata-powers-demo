import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class AestheticComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="res-item">
                    <div className="res-val">
                        {
                            this.props.data.wordCloud.map((wc,idx)=>{
                                return(
                                    <div className="rtag small" key={idx}>
                                        {wc}
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
                {
                    Object.keys(this.props.data).map((k,i)=>{
                        let curObj = this.props.data[k];
                        if(k !== "wordCloud" && k !== "tags"){
                            return(
                                <div className="res-item half" key={i}>
                                    <div className="res-label">{k}</div>
                                    <div className="res-text">
                                        {curObj.text[0]}
                                        <span>
                                            {
                                                curObj.score ? " (" + (curObj.score * 100).toFixed(1) + "%)" : ""
                                            }
                                        </span>
                                    </div>
                                    <div className="res-val">
                                        <LinearProgress value={curObj.score}/>
                                    </div>
                                </div>
                            )
                        }
                    })
                }
            </div>
        )
    }
}