import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class CarPoseComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="comp-title">Car Pose Detection</div>
                {
                    this.props.data.pose ?
                    <div>
                        {
                            this.props.data.pose.map((sc,idx)=>{
                                return(
                                    <div className="res-item half" key={idx}>
                                        <div className="res-label">{sc[0]}</div>
                                        <div className="res-text">
                                            {
                                                " (" + (sc[1] * 100).toFixed(1) + "%)"
                                            }
                                        </div>
                                        <div className="res-val">
                                            <LinearProgress value={sc[1]}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div> 
                    :
                    null       
                }
                
            </div>
        )
    }
}