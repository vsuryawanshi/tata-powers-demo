import React, {Component} from "react";

export default class WeatherComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="res-item">
                    <div className="res-label">Weather</div>
                    {
                        this.props.data != null && this.props.data.weather ?
                        <div className="res-val">
                        {
                            this.props.data.weather.map((w,idx)=>{
                                return(
                                    <div className="rtag" key={idx}>{w}</div>
                                )
                            })
                        }
                    </div>
                    :
                    <div className="comp-error">No Weather data found</div>
                    }
                </div>
            </div>
        )
    }
}