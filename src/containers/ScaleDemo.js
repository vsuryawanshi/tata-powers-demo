import React, { Component } from "react";
import axios from "axios";
import Draggable from 'react-draggable';

import RowItem from "../common/RowItem";

const DEFAULT_IMAGES = [
    "http://62.210.100.210:8099/sample/108.jpeg",
    "http://62.210.100.210:8099/sample/149.jpeg",
    "http://62.210.100.210:8099/sample/250.jpeg",
    "http://62.210.100.210:8099/sample/50.jpeg",
    "http://62.210.100.210:8099/sample/586.jpeg",
    "http://62.210.100.210:8099/sample/591.jpeg",
    "http://62.210.100.210:8099/sample/599.jpeg",
    "http://62.210.100.210:8099/sample/604.jpeg",
    "http://62.210.100.210:8099/sample/124.jpeg",
    "http://62.210.100.210:8099/sample/200.jpeg",
    "http://62.210.100.210:8099/sample/395.jpeg",
    "http://62.210.100.210:8099/sample/55.jpeg",
    "http://62.210.100.210:8099/sample/589.jpeg",
    "http://62.210.100.210:8099/sample/596.jpeg",
    "http://62.210.100.210:8099/sample/602.jpeg"
];


export default class ScaleDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiCallInProgress:false,
            selectedTypeIndex:0,
            selectedTabIndex:0,
            selectedImageIndex:-1,
            imgUrl:"",
            showJSONContent:false,
            currentImageData:null,
            showModal:false,
            currentBaseUrl:"",
            showResults:false,
            selectedMainCategoryIndex:0,
            imgErr:""
        };
        this.imageUrlText = null;
    }

    makeApiCall(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://51.15.15.25:8020/api/v1/meter?url=" +this.state.imgUrl
            }).then(response => {
                if(response.data.meta._code !== 200){
                    this.setState({
                        currentImageData:null,
                        apiCallInProgress:false,
                        showResults:false,
                        showJSONContent:false,
                        imgErr:response.data.meta.message
                    })
                } else {
                    this.setState({
                        currentImageData:response.data,
                        apiCallInProgress:false,
                        showResults:false,
                        imgErr:""
                    })
                }
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    validateUrlandMakeCall(){
        var pastedUrl = this.imageUrlText.value;
        this.binaryImageData = null;
        this.setState({
            imgUrl:pastedUrl,
            showModal:false,
        },()=>{
            this.makeApiCall();
        })
    }

    showUploadedItem (source) {
        this.setState({imgUrl:source});
    }

    makeUploadRequest(fdata){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"POST",
                url:"http://51.15.15.25:8020/api/v1/meter",
                data:{"image":fdata}
            }).then(response =>{
                if(response.data.meta._code !== 200){
                    this.setState({
                        currentImageData:null,
                        apiCallInProgress:false,
                        showResults:false,
                        showJSONContent:false,
                        imgErr:response.data.meta._message
                    })
                } else {
                    this.setState({
                        currentImageData:response.data,
                        apiCallInProgress:false,
                        showResults:false,
                        imgErr:""
                    })
                }
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    render() {
        return (
            <div className="home-wrapper">
                <div className="title" onClick={()=>{
                    this.getDataFromCanvas();
                }}>Meter Reading Detection API</div>
                <div className="subtitle">Using computer vision, our API extracts the digital information from the images and makes it useful</div>

                <div className="main-section">
                    <div className="left-input">
                        <div className="btn-input">
                            <button className="btn black big" style={{marginRight:20}} onClick={()=>{
                                this.setState({
                                    showModal:true
                                },()=>{
                                    this.imageUrlText.focus();
                                });
                            }}>Enter Image URL</button>
                            <div className="upload">
                                <input type="file" name="file" id="file" className="inputfile" accept="image/*" onChange={(e)=>{
                                    var _this = this;
                                    if(e.target.files.length > 0){
                                        var file = e.target.files[0];
                                        var formdata;
                                        if (window.FormData) {
                                            formdata = new FormData();
                                        }
                                        if(window.FileReader){
                                            var reader = new FileReader();
                                            reader.onloadend = function (e) { 
                                                _this.showUploadedItem(e.target.result);
                                                if (formdata) {
                                                    formdata.append("image", file);
                                                }
                                                var be = e.target.result.substring(e.target.result.indexOf(",")+1);
                                                _this.binaryImageData = be;
                                                _this.makeUploadRequest(_this.binaryImageData);
                                            };
                                            reader.readAsDataURL(file);
                                        }
                                    }
                                }}/>
                                <label htmlFor="file">Upload an Image</label>
                            </div> 
                        </div>
                        <div className="image-input-container" style={{marginTop:30}}>
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c"/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext">Please choose an image</div>
                            </div>
                            <div className="default-images">
                                {
                                    DEFAULT_IMAGES.map((currentImg,index)=>{
                                        return(
                                            <div 
                                                className={`default-img` + (this.state.selectedImageIndex == index ? " active" : "")} 
                                                key={index} 
                                                onClick={()=>{
                                                    this.binaryImageData = null;
                                                    this.setState({
                                                        selectedImageIndex:index,
                                                        imgUrl:currentImg,
                                                        showJSONContent:false,
                                                        showResults:false,
                                                        currentImageData:null
                                                    },()=>{
                                                        this.makeApiCall();
                                                    });
                                                }}>
                                                <img src={currentImg} className="sqimg"/>    
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        {
                            this.state.imgUrl !== "" ?
                            <div className="current-selected-image">
                                <img src={this.state.imgUrl} className="mimg" ref={node => this.srcImage = node}/>
                                {
                                    this.state.imgErr !== "" ?
                                    <div className="ier">
                                        {this.state.imgErr}
                                    </div>
                                    :
                                    null
                                }
                                <button className="btn jbtn black" onClick={()=>{
                                    this.setState({
                                        showResults:true
                                    });
                                }}>Show Image</button>
                                <button className="btn jbtn" onClick={()=>{
                                    this.setState({
                                        showJSONContent:true
                                    });
                                }}>JSON</button>
                            </div>
                            :
                            null
                        }
                    </div>
                    <div className="result-container">
                        <div className={`results-container`}>
                            <div className="titlebar">
                                <div className="ttext ew">API Result</div>
                            </div>
                            {
                                this.state.apiCallInProgress ?
                                <div className="loader"/>
                                :
                                null
                            }
                            {
                                this.state.currentImageData !== null?
                                <div className="result-content">
                                    <div className="row">
                                        <div className="lbl">Reading : </div>
                                        <div className="val">{this.state.currentImageData.tags.reading}</div>
                                    </div>

                                    <div className="row">
                                        <div className="lbl">Serial Number : </div>
                                        <div className="val">{this.state.currentImageData.tags.serialNumber}</div>
                                    </div>

                                    <div className="class-container">
                                        {
                                            this.state.currentImageData.tags.classes.map((cl,index)=>{
                                                return (
                                                    <RowItem  
                                                        key={index}
                                                        data={cl} 
                                                        imageRef={this.srcImage} 
                                                        ow={this.state.currentImageData.tags.width} 
                                                        oh={this.state.currentImageData.tags.height}
                                                        iu={this.state.imgUrl}/>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                                :
                                <div className="idle-message">Please select an image to see results</div>
                            }
                        </div>       
                    </div>
                </div>
                <div className={`modal` + (this.state.showModal ? " show" : "")}>
                    <div className="mtitle">Enter Image URL</div>
                    <div className="mcontent">
                        <textarea className="imgurl" ref={node => this.imageUrlText = node} placeholder="Paste the image url here"/>
                    </div>
                    <div className="maction">
                        <button className="btn" onClick={()=>{
                            this.setState({
                                showModal:false
                            })
                        }} style={{marginRight:20}}>Cancel</button>
                        <button className="btn black" onClick={()=>{
                            this.validateUrlandMakeCall();
                        }}>Go</button>
                    </div>
                </div>
                <Draggable
                    handle=".handle"
                    defaultPosition={{x: 600, y: -100}}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                        <div className={`json-container` + (this.state.showJSONContent ? " show" : "")}>
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c" onClick={()=>{
                                        this.setState({
                                            showJSONContent:false
                                        })
                                    }}/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext handle">Response JSON</div>
                            </div>
                            <div className="json-content">
                                <pre className="javascript">
                                    {JSON.stringify(this.state.currentImageData, null, 4)}
                                </pre>
                            </div>
                        </div>
                </Draggable>
                <Draggable
                    handle=".ew"
                    defaultPosition={{x: 600, y: -100}}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                        <div className={`outputimage-container` + (this.state.showResults ? " show" : "")}>
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c" onClick={()=>{
                                        this.setState({
                                            showResults:false
                                        })
                                    }}/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext ew">Result Image</div>
                            </div>
                            {
                                this.state.currentImageData !== null?
                                <div className="result-content">
                                    <img src={"data:image/png;base64, " + this.state.currentImageData.tags.image} className="its"/>
                                </div>
                                :
                                null
                            }
                        </div>
                </Draggable>
            </div>
        );
    }
}