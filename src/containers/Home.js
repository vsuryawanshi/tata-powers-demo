import React, { Component } from "react";
import TextAnimate from "./TextAnimate";
import ScaleDemo from "./ScaleDemo";

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            counter: 0 
        };
    }

    render() {
        return (
            <div className="wrapper">
                <div className="header-wrap">
                    <header className="sheader">
                        <div className="s-container headers">
                            <div className="header-container">
                                <div className="logo">
                                    <img src={require("../images/deeppulse.svg")} style={{width:200}}/>
                                    
                                </div>
                                <div className="nav">
                                    <TextAnimate items={["Think like humans","Work like machines"]} timeout={3000} ref="taHead"/>
                                </div>
                                <div className="logo" >
                                    <img src={require("../images/tlogo.png")} style={{width:120}}/>
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
                <div className="demo">
                    <ScaleDemo/>
                </div>
            </div>
        );
    }
}